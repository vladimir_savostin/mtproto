package mtproto

import (
	"errors"
	"fmt"
	"github.com/astaxie/beego/logs"
	"math/rand"
	"net"
	"os"
	"runtime"
	"sync"
	"time"
)

var (
	ErrDifferenceTooLong = errors.New("the update difference is too long, please try to set new pts")
)

type MTProto interface {
	Connect() error
	Close() error
	//GetSession - get current session
	GetSession() *Session
	//SetSession - set session for current client
	SetSession(session *Session)
	//GetUpdates - returns a chan into which all updates will be sent
	GetUpdates() chan TL
	//AuthCheckPhone - return true if the phone is registered
	AuthCheckPhone(phoneNumber string) (phoneRegistered bool, err error)
	//AuthSendCode - send code to the phone number (this code and retrieved code hash used in SignIn method)
	AuthSendCode(phoneNumber string) (codeHash string, err error)
	//SignIn - sign in to the telegram (current session may be retrieved through GetSession)
	AuthSignIn(phoneNumber, code, codeHash string) (Account, error)
	//GetDialogs - get all user dialogs
	GetDialogs(limit int32) (Dialogs, error)
	//GetState - get current updatesState of updates from server
	GetState() (updatesState, error)
	//SyncDifference - actualize updates resolution from remote server and process missed updates
	SyncDifference() error
	//GetDifference - get all missed updates from remote server
	GetDifference() (TL, error)
	//GetContacts - get all user contacts
	GetContacts() error
	//SendToUser - sends message to provided user
	SendToUser(userId int32, accessHash int64, msg string) error
	//SendToChat - sends message to provided chat
	SendToChat(chatId int32, msg string) error
}

//todo check send and read errors count
type Proto struct {
	conf Conf

	conn         *net.TCPConn
	updates      chan TL //chan which to sent all proceeded updates
	updatesQueue []TL    //used to store new updates which could not been sent to the updates chan (when it nil e.g.)
	queueSend    chan packetToSend
	stopSend     chan struct{} //when closes the Proto stops send routine
	stopRead     chan struct{} //when closes the Proto stops read routine
	stopPing     chan struct{} //when closes the Proto stops ping routine
	allDone      chan struct{}
	msgsIdToAck  map[int64]packetToSend
	msgsIdToResp map[int64]chan TL
	isSyncing    bool //is true when sync difference (recovering gap) is running

	session *Session

	mutex *sync.Mutex

	dclist map[int32]string
}

func NewMTProto(conf Conf) (*Proto, error) {
	m := new(Proto)
	m.session = new(Session)

	m.conf.AppID = conf.AppID
	m.conf.AppHash = conf.AppHash
	m.conf.TlgAddr = conf.TlgAddr
	m.conf.Timeout = conf.Timeout
	m.session.Encrypted = false
	rand.Seed(time.Now().UnixNano())
	m.session.ID = rand.Int63()
	return m, nil
}

func (m *Proto) Connect() error {
	logs.Debug("start to connect client")
	var err error
	var tcpAddr *net.TCPAddr

	// connect
	tcpAddr, err = net.ResolveTCPAddr("tcp", m.conf.TlgAddr)
	if err != nil {
		return err
	}
	logs.Debug("%+v %+v", tcpAddr, err)
	m.conn, err = net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return err
	}
	logs.Debug("%+v %+v", m.conn, err)
	_, err = m.conn.Write([]byte{0xef})
	if err != nil {
		return err
	}
	logs.Debug("%+v %+v", m.conn, err)
	// get new AuthKey if need
	if !m.session.Encrypted {
		err = m.makeAuthKey()
		if err != nil {
			return err
		}
	}
	// start goroutines
	m.queueSend = make(chan packetToSend, 64)
	m.stopSend = make(chan struct{})
	m.stopRead = make(chan struct{})
	m.stopPing = make(chan struct{})
	m.allDone = make(chan struct{})
	m.msgsIdToAck = make(map[int64]packetToSend)
	m.msgsIdToResp = make(map[int64]chan TL)
	m.mutex = &sync.Mutex{}
	go m.sendRoutine()
	go m.readRoutine()

	var resp chan TL
	var x TL

	// (help_getConfig)
	resp = make(chan TL, 1)
	m.queueSend <- packetToSend{
		TL_invokeWithLayer{
			layer,
			TL_initConnection{
				m.conf.AppID,
				"server",
				runtime.GOOS + "/" + runtime.GOARCH,
				"0.0.4",
				"en",
				TL_help_getConfig{},
			},
		},
		resp,
	}
	x = <-resp
	switch x.(type) {
	case TL_config:
		m.dclist = make(map[int32]string, 5)
		for _, v := range x.(TL_config).dc_options {
			v := v.(TL_dcOption)
			m.dclist[v.id] = fmt.Sprintf("%s:%d", v.ip_address, v.port)
		}
	default:
		return fmt.Errorf("Got: %T", x)
	}

	// start keepalive pinging
	go m.pingRoutine()

	return nil
}

func (m *Proto) reconnect(addr string) error {
	logs.Warn("begin trying to reconnect")
	err := m.Close()
	if err != nil {
		return fmt.Errorf("close proto client:%v", err)
	}
	m.conf.TlgAddr = addr
	return m.Connect()
}

//Close - closes connection
func (m *Proto) Close() error {
	logs.Info("close connection")
	// stop ping routine
	m.stopPing <- struct{}{}
	close(m.stopPing)

	// stop send routine
	m.stopSend <- struct{}{}
	close(m.stopSend)

	// stop read routine
	m.stopRead <- struct{}{}
	close(m.stopRead)

	//wait signals from ping, send and read routines
	<-m.allDone
	<-m.allDone
	<-m.allDone

	// close connection
	logs.Debug("**********************close")
	err := m.conn.Close()
	if err != nil {
		return err
	}

	// close send queue
	close(m.queueSend)

	return nil
}

//GetSession - return current session
func (m Proto) GetSession() *Session {
	return m.session
}

func (m *Proto) SetSession(session *Session) {
	m.session = session
}

func (m *Proto) GetUpdates() (updates chan TL) {
	if m.updates == nil {
		m.updates = make(chan TL, 10)
	}
	//send all accumulated updates to new chan
	go func() {
		for _, u := range m.updatesQueue {
			m.updates <- u
		}
	}()
	return m.updates
}

func (m *Proto) AuthCheckPhone(phoneNumber string) (phoneRegistered bool, err error) {

	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{
		TL_auth_checkPhone{
			phoneNumber}, resp}
	x := <-resp
	logs.Debug("%T  %+v", x, x)
	switch x.(type) {
	case TL_auth_checkedPhone:
		var phone TL_auth_checkedPhone
		phone = x.(TL_auth_checkedPhone)
		logs.Debug("registered %T", phone.phone_registered)
		logs.Debug("invited %T", phone.phone_invited)
		return toBool(phone.phone_registered), nil
	case TL_rpc_error:
		x := x.(TL_rpc_error)
		if x.error_code != 303 {
			return false, fmt.Errorf("RPC error_code: %d; msg:%s", x.error_code, x.error_message)
		}
		var newDc int32
		n, _ := fmt.Sscanf(x.error_message, "PHONE_MIGRATE_%d", &newDc)
		if n != 1 {
			n, _ := fmt.Sscanf(x.error_message, "NETWORK_MIGRATE_%d", &newDc)
			if n != 1 {
				return false, fmt.Errorf("RPC error_string: %s", x.error_message)
			}
		}

		newDcAddr, ok := m.dclist[newDc]
		if !ok {
			return false, fmt.Errorf("Wrong DC index: %d", newDc)
		}
		err := m.reconnect(newDcAddr)
		if err != nil {
			return false, err
		}
	default:
		return false, fmt.Errorf("error: want: %T; got: %T",
			TL_auth_checkedPhone{}, x)
	}
	return false, fmt.Errorf("check phone: unknown error")
}

func (m *Proto) AuthSendCode(phoneNumber string) (codeHash string, err error) {

	phoneRegistered := false
	flag := true
	for flag {
		resp := make(chan TL, 1)
		m.queueSend <- packetToSend{
			TL_auth_sendCode{
				phoneNumber,
				0,
				m.conf.AppID,
				m.conf.AppHash,
				"en"},
			resp}
		x := <-resp
		logs.Debug("%T  %+v", x, x)
		switch x.(type) {
		case TL_auth_sentCode:
			var authSentCode TL_auth_sentCode
			authSentCode = x.(TL_auth_sentCode)
			codeHash = authSentCode.phone_code_hash
			phoneRegistered = toBool(authSentCode.phone_registered)
			flag = false
			logs.Debug("%T", authSentCode.phone_registered)
		case TL_auth_sentAppCode:
			var authSentCode TL_auth_sentAppCode
			authSentCode = x.(TL_auth_sentAppCode)
			codeHash = authSentCode.phone_code_hash
			phoneRegistered = toBool(authSentCode.phone_registered)
			flag = false
		case TL_rpc_error:
			x := x.(TL_rpc_error)
			if x.error_code != 303 {
				return "", fmt.Errorf("RPC error_code: %d; msg:%s", x.error_code, x.error_message)
			}
			var newDc int32
			n, _ := fmt.Sscanf(x.error_message, "PHONE_MIGRATE_%d", &newDc)
			if n != 1 {
				n, _ := fmt.Sscanf(x.error_message, "NETWORK_MIGRATE_%d", &newDc)
				if n != 1 {
					return "", fmt.Errorf("RPC error_string: %s", x.error_message)
				}
			}

			newDcAddr, ok := m.dclist[newDc]
			if !ok {
				return "", fmt.Errorf("Wrong DC index: %d", newDc)
			}
			err := m.reconnect(newDcAddr)
			if err != nil {
				return "", err
			}
		default:
			return "", fmt.Errorf("error: want: %T or %T; got: %T",
				TL_auth_sentCode{}, TL_auth_sentAppCode{}, x)
		}
	}
	//check if the phone is registered
	if !phoneRegistered {
		return "", fmt.Errorf("phone number %s is not registered in telegram", phoneNumber)
	}

	return codeHash, nil
}

func (m *Proto) AuthSignIn(phoneNumber, code, codeHash string) (account Account, err error) {
	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{
		TL_auth_signIn{phoneNumber, codeHash, code},
		resp,
	}
	x := <-resp
	auth, ok := x.(TL_auth_authorization)
	if !ok {
		return account, fmt.Errorf("RPC: %#v", x)
	}
	userSelf, ok := auth.user.(TL_userSelf)
	if !ok {
		return account, fmt.Errorf("RPC: %#v", x)
	}
	logs.Debug("%+v", userSelf)
	logs.Debug("%+T", userSelf.photo)
	account.ID = userSelf.id
	account.FName = userSelf.first_name
	account.LName = userSelf.last_name
	account.UserName = userSelf.username
	//todo handle photo
	//switch userSelf.photo.(type) {
	//case TL_userProfilePhoto:
	//	photo := userSelf.photo.(TL_userProfilePhoto)
	//	logs.Debug("photo:%+v", photo)
	//	switch photo.photo_small.(type) {
	//	case TL_fileLocation:
	//		loc := photo.photo_small.(TL_fileLocation)
	//		logs.Debug("loc:%+v", loc)
	//	case TL_fileLocationUnavailable:
	//		loc := photo.photo_small.(TL_fileLocationUnavailable)
	//		logs.Debug("loc:%+v", loc)
	//	}
	//case TL_userProfilePhotoEmpty:
	//	photo := userSelf.photo.(TL_userProfilePhoto)
	//	logs.Debug("photo:%+v", photo)
	//}

	//get current resolution for newly authorized user and set it to the session
	state, err := m.GetState()
	if err != nil {
		return account, err
	}
	m.session.setState(state)

	return account, nil
}

//GetState - get updates resolution for current session
func (m *Proto) GetState() (updatesState, error) {
	logs.Debug("get state request")
	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{TL_updates_getState{}, resp}
	x := <-resp
	logs.Debug("get state response:%+v", x)
	s, ok := x.(TL_updates_state)
	if !ok {
		return updatesState{}, fmt.Errorf("RPC: %#v", x)
	}
	return updatesState{Pts: s.pts, Qts: s.qts, Date: s.date, Seq: s.seq, UnreadCount: s.unread_count}, nil
}

//todo return contacts
func (m *Proto) GetContacts() error {
	//todo check response type
	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{TL_contacts_getContacts{""}, resp}
	x := <-resp
	list, ok := x.(TL_contacts_contacts)
	if !ok {
		return fmt.Errorf("RPC: %#v", x)
	}

	contacts := make(map[int32]TL_userContact)
	for _, v := range list.users {
		if v, ok := v.(TL_userContact); ok {
			contacts[v.id] = v
		}
	}
	fmt.Printf(
		"\033[33m\033[1m%10s    %10s    %-30s    %-20s\033[0m\n",
		"id", "mutual", "name", "username",
	)
	for _, v := range list.contacts {
		v := v.(TL_contact)
		fmt.Printf(
			"%10d    %10t    %-30s    %-20s\n",
			v.user_id,
			toBool(v.mutual),
			fmt.Sprintf("%s %s", contacts[v.user_id].first_name, contacts[v.user_id].last_name),
			contacts[v.user_id].username,
		)
	}

	return nil
}

//GetDialogs - get current user's first limit dialogs/chats with related users
func (m *Proto) GetDialogs(limit int32) (dialogs Dialogs, err error) {
	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{
		TL_messages_getDialogs{
			offset: 0,      //Number of list elements to be skipped
			max_id: 0,      //If a positive value was transmitted sent, the method will return only dialogs with IDs less than the set one
			limit:  limit}, //Number of list elements to be returned
		resp}
	x := <-resp
	logs.Debug("%#v", x)
	switch x.(type) {
	case TL_messages_dialogs:
		var tlDialogs TL_messages_dialogs
		tlDialogs = x.(TL_messages_dialogs)
		for _, dialog := range tlDialogs.dialogs {
			dialogs.appendDialog(dialog)
		}
		for _, chat := range tlDialogs.chats {
			dialogs.appendChat(chat)
		}
		for _, user := range tlDialogs.users {
			dialogs.appendUser(user)
		}
	case TL_messages_dialogsSlice:
		var tlDialogs TL_messages_dialogsSlice
		tlDialogs = x.(TL_messages_dialogsSlice)
		for _, dialog := range tlDialogs.dialogs {
			dialogs.appendDialog(dialog)
		}
		for _, chat := range tlDialogs.chats {
			dialogs.appendChat(chat)
		}
		for _, user := range tlDialogs.users {
			dialogs.appendUser(user)
		}
	default:
		return dialogs, fmt.Errorf("wornge rpc response: %#v", x)
	}
	return dialogs, nil
}

//SendToUser - sends msg to provided user
func (m *Proto) SendToChat(chatId int32, msg string) error {
	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{
		TL_messages_sendMessage{
			TL_inputPeerChat{chatId},
			msg,
			rand.Int63(),
		},
		resp,
	}
	x := <-resp
	_, ok := x.(TL_messages_sentMessage)
	if !ok {
		return fmt.Errorf("RPC: %#v", x)
	}

	return nil
}

//SendToUser - sends msg to provided user
func (m *Proto) SendToUser(userId int32, accessHash int64, msg string) error {
	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{
		TL_messages_sendMessage{
			TL_inputPeerForeign{userId, accessHash},
			msg,
			rand.Int63(),
		},
		resp,
	}
	x := <-resp
	_, ok := x.(TL_messages_sentMessage)
	if !ok {
		return fmt.Errorf("RPC: %#v", x)
	}

	return nil
}

func (m *Proto) GetUserInfo(userId int32) error {
	resp := make(chan TL, 1)
	m.queueSend <- packetToSend{
		TL_users_getFullUser{
			//TL_inputPeerChat{user_id},
			//msg,
			//rand.Int63(),
		},
		resp,
	}
	x := <-resp
	_, ok := x.(TL_messages_sentMessage)
	if !ok {
		return fmt.Errorf("RPC: %#v", x)
	}

	return nil
}

func (m *Proto) pingRoutine() {
	for {
		select {
		case <-m.stopPing:
			logs.Debug("stop ping routine")
			m.allDone <- struct{}{}
			return
		case <-time.After(60 * time.Second):
			m.queueSend <- packetToSend{TL_ping{0xCADACADA}, nil}
		}
	}
}

func (m *Proto) sendRoutine() {
	for {
		select {
		case <-m.stopSend:
			logs.Debug("stop send routine")
			m.allDone <- struct{}{}
			return
		case x := <-m.queueSend:
			err := m.sendPacket(x.msg, x.resp)
			if err != nil {
				logs.Error("SendRoutine:", err)
				os.Exit(2)
			}
		}
	}
}

func (m *Proto) readRoutine() {
	//start to read in goroutine and send data to the chan
	data := make(chan interface{}, 1)
	defer close(data)
	go func() {
		for {
			d, err := m.read(m.conf.Timeout)
			logs.Debug("read fom conn %#v", d)
			if d == nil {
				logs.Info("data from conn == nil (connection closed), err=%v", err)
				select {
				case <-m.stopRead: //stopRead is closed, so client is shutting down, return
					return
				default: //in any cases try to reconnect
					err := m.reconnect(m.conf.TlgAddr)
					if err != nil {
						logs.Critical("trying to reconnect:%+v", err)
					}
					return
				}
			}
			if err != nil {
				logs.Error("read from conn:%v", err)
			}
			data <- d
		}
	}()

	//listen stop and data channels and do stuff
	for {
		select {
		case <-m.stopRead:
			logs.Debug("stop read routine")
			m.allDone <- struct{}{}
			return
		case d := <-data:
			go m.process(m.session.MsgId, m.session.SeqNo, d)
		}
	}
}

func (m *Proto) process(msgId int64, seqNo int32, data interface{}) interface{} {
	logs.Debug("process new data %#v", data)
	switch data.(type) {
	//any not important updates below:
	case TL_updateShort:
		update := data.(TL_updateShort)
		if m.isSyncing || m.needApplyUpdate(update) {
			m.sendUpdate(update)
		}
	//any important pts, qts updates below:
	case TL_updateShortMessage:
		update := data.(TL_updateShortMessage)
		if m.isSyncing || m.needApplyUpdate(update) {
			m.sendUpdate(update)
		}
	case TL_updateShortChatMessage:
		update := data.(TL_updateShortChatMessage)
		if m.isSyncing || m.needApplyUpdate(update) {
			m.sendUpdate(update)
		}
	case TL_updateNewMessage:
		update := data.(TL_updateNewMessage)
		if m.isSyncing || m.needApplyUpdate(update) {
			m.sendUpdate(update)
		}
	case TL_updateNewEncryptedMessage:
		update := data.(TL_updateNewEncryptedMessage)
		if m.isSyncing || m.needApplyUpdate(update) {
			m.sendUpdate(update)
		}
	case TL_updateReadMessages:
		update := data.(TL_updateReadMessages)
		if m.isSyncing || m.needApplyUpdate(update) {
			m.sendUpdate(update)
		}
	case TL_updateDeleteMessages:
		update := data.(TL_updateDeleteMessages)
		if m.isSyncing || m.needApplyUpdate(update) {
			m.sendUpdate(update)
		}
	//any important seq updates below:
	case TL_updates: //list of updates listed above
		updates := data.(TL_updates)
		for _, update := range updates.updates {
			m.process(m.session.MsgId, m.session.SeqNo, update)
		}
		result := m.checkState(updates)
		logs.Debug(result)
		logs.Debug("new local state: %+v", m.session.UpdatesState)
		if result == gap { //if there seq gap - need to get diff
			err := m.SyncDifference()
			if err != nil {
				logs.Error(err)
			}
		}
	case TL_updatesCombined:
		updates := data.(TL_updatesCombined)
		for _, update := range updates.updates {
			m.process(m.session.MsgId, m.session.SeqNo, update)
		}
		result := m.checkState(updates)
		logs.Debug(result)
		logs.Debug("new local state: %+v", m.session.UpdatesState)
		if result == gap { //if there seq gap - need to get diff
			err := m.SyncDifference()
			if err != nil {
				logs.Error(err)
			}
		}
	//any important updates which do not demand state handling
	case TL_message: //most likely returned from getDifference method
		update := data.(TL_message)
		//m.checkState(update) - ignore processing of resolution
		m.sendUpdate(update)
	//any service messages below:
	case TL_msg_container:
		items := data.(TL_msg_container).items
		for _, v := range items {
			m.process(v.msg_id, v.seq_no, v.data)
		}
	case TL_bad_server_salt:
		data := data.(TL_bad_server_salt)
		m.session.ServerSalt = data.new_server_salt
		m.mutex.Lock()
		for k, v := range m.msgsIdToAck {
			delete(m.msgsIdToAck, k)
			m.queueSend <- v
		}
		m.mutex.Unlock()
		//bad msg - try to fix error with session
	case TL_crc_bad_msg_notification:
		data := data.(TL_crc_bad_msg_notification)
		//todo handle all err codes
		switch data.error_code {
		case badMsgLowSeqNo:
			m.session.LastSeqNo += 4
			m.mutex.Lock()
			for k, v := range m.msgsIdToAck {
				delete(m.msgsIdToAck, k)
				m.queueSend <- v
			}
			m.mutex.Unlock()
		}
	case TL_new_session_created:
		data := data.(TL_new_session_created)
		m.session.ServerSalt = data.server_salt
	case TL_ping:
		data := data.(TL_ping)
		m.queueSend <- packetToSend{TL_pong{msgId, data.ping_id}, nil}
	case TL_pong: // (ignore)
	case TL_msgs_ack:
		data := data.(TL_msgs_ack)
		m.mutex.Lock()
		for _, v := range data.msgIds {
			delete(m.msgsIdToAck, v)
		}
		m.mutex.Unlock()
	case TL_rpc_result:
		data := data.(TL_rpc_result)
		logs.Debug("new rpc result %+v", data)
		x := m.process(msgId, seqNo, data.obj)
		m.mutex.Lock()
		v, ok := m.msgsIdToResp[data.req_msg_id]
		if ok {
			if x != nil {
				v <- x.(TL)
			}
			close(v)
			delete(m.msgsIdToResp, data.req_msg_id)
		}
		delete(m.msgsIdToAck, data.req_msg_id)
		m.mutex.Unlock()

	default:
		return data

	}

	if (seqNo & 1) == 1 {
		m.queueSend <- packetToSend{TL_msgs_ack{[]int64{msgId}}, nil}
	}

	return nil
}

//needApplyUpdate - checks state and returns true if update may be applied
//if there gap, recovers the gap and returns false
//return false for already proceeded updates
func (m *Proto) needApplyUpdate(update Update) bool {
	result := m.checkState(update)
	logs.Debug(result)
	logs.Debug("new local state: %+v", m.session.UpdatesState)
	switch result {
	case apply:
		return true
	case gap:
		err := m.SyncDifference()
		if err != nil {
			logs.Error(err)
		}
		return false
	case ignore:
		return false
	default:
		logs.Warn("unknown check state result:'%v'", result)
		return false
	}
}

//checkState - properly services resolution (pts, qts, seq, date params) from update
func (m *Proto) checkState(update Update) resolution {
	logs.Info("check state for new update: type:%s %#v\n current state%+v",
		update.getType(), update, m.session.UpdatesState)
	switch update.getType() {
	//todo handle seq in pts (some pts updates have seq param also)
	case pts: //process new message box update
		u := update.(UpdatePts)
		if u.getPts() != 0 { //check pts
			if m.session.UpdatesState.Pts+u.getPtsCount() == u.getPts() {
				m.session.UpdatesState.Pts = u.getPts()
				//if ok, then check seq (if presented)
				if u.getSeq() > 0 {
					if m.session.UpdatesState.Seq+1 == u.getSeq() {
						m.session.UpdatesState.Seq = u.getSeq()
					} else if m.session.UpdatesState.Seq+1 < u.getSeq() {
						return gap
					}
				}
				return apply
			} else if m.session.UpdatesState.Pts+u.getPtsCount() > u.getPts() {
				return ignore
			} else if m.session.UpdatesState.Pts+u.getPtsCount() < u.getPts() {
				return gap
			}
		}

	case qts: //ignore new secret message box update
		u := update.(UpdateQts)
		m.session.UpdatesState.Qts = u.getQts()
	case seq: //process new seq update
		u := update.(UpdateSeq)
		seqCount := u.getSeq() - u.getStartSeq()
		if seqCount <= 0 {
			seqCount = 1
		}
		if m.session.UpdatesState.Seq+seqCount == u.getSeq() {
			m.session.UpdatesState.Seq = u.getSeq()
			return apply
		} else if m.session.UpdatesState.Seq+seqCount > u.getSeq() {
			return ignore
		} else if m.session.UpdatesState.Seq+seqCount < u.getSeq() {
			return gap
		}
	case other:
		logs.Debug("do not process 'other' update %+v", update)
	default:
		logs.Warn("unknown update: %#v", update)

	}
	if date := update.getDate(); date != 0 {
		m.session.UpdatesState.Date = date
	}
	return ignore
}

//SyncDifference  - recovers a gap  of missed updates
func (m *Proto) SyncDifference() error {
	if m.isSyncing {
		logs.Warn("updates syncing is already running")
		return nil
	}
	//set is_syncing to true to block all updates state check
	m.setIsSyncing(true)
	defer m.setIsSyncing(false)
	logs.Debug("start to sync difference")
	for {
		logs.Debug("get difference")
		d, err := m.GetDifference()
		if err != nil {
			return err
		}
		logs.Debug("difference is: %#v", d)
		switch d.(type) {
		case TL_updates_differenceEmpty: //No events.
			diff := d.(TL_updates_differenceEmpty)
			m.session.UpdatesState.Date = diff.date
			m.session.UpdatesState.Seq = diff.seq
			logs.Debug("synced to new state: %v", m.session.UpdatesState)
			return nil
		case TL_updates_difference: //Full list of occurred events.
			diff := d.(TL_updates_difference)
			for _, u := range diff.new_messages {
				m.process(m.session.MsgId, m.session.SeqNo, u)
			}
			for _, u := range diff.other_updates {
				m.process(m.session.MsgId, m.session.SeqNo, u)
			}
			newState, ok := diff.state.(TL_updates_state)
			if !ok {
				return fmt.Errorf("TL_updates_difference wrong struct: %#v", diff)
			}
			m.session.setState(updatesState{
				Pts:         newState.pts,
				Qts:         newState.qts,
				Date:        newState.date,
				Seq:         newState.seq,
				UnreadCount: newState.unread_count,
				ChannelsPts: nil,
			})
			logs.Debug("synced to new state: %v", m.session.UpdatesState)
			return nil
		case TL_updates_differenceSlice: //Incomplete list of occurred events.
			diff := d.(TL_updates_differenceSlice)
			for _, u := range diff.new_messages {
				m.process(m.session.MsgId, m.session.SeqNo, u)
			}
			for _, u := range diff.other_updates {
				m.process(m.session.MsgId, m.session.SeqNo, u)
			}
			intermediateState, ok := diff.intermediate_state.(TL_updates_state)
			if !ok {
				return fmt.Errorf("TL_updates_difference wrong struct: %#v", diff)
			}
			m.session.setState(updatesState{
				Pts:         intermediateState.pts,
				Qts:         intermediateState.qts,
				Date:        intermediateState.date,
				Seq:         intermediateState.seq,
				UnreadCount: intermediateState.unread_count,
				ChannelsPts: nil,
			})
			logs.Debug("synced to new state: %v", m.session.UpdatesState)
			continue //keep true to fetch next portion of difference
		case TL_updatesTooLong:
			return ErrDifferenceTooLong
		}
	}
	return nil
}

//GetDifference - get updates difference from mtproto server
func (m *Proto) GetDifference() (TL, error) {
	resp := make(chan TL, 1)
	var date int32
	if m.session.UpdatesState.Date == 0 {
		date = int32(time.Now().Unix())
	} else {
		date = m.session.UpdatesState.Date
	}
	m.queueSend <- packetToSend{
		TL_updates_getDifference{
			pts:  m.session.UpdatesState.Pts,
			qts:  m.session.UpdatesState.Qts,
			date: date,
		},
		resp,
	}
	x := <-resp
	switch x.(type) {
	case TL_updates_difference:
	case TL_updates_differenceEmpty:
	case TL_updates_differenceSlice:
	case TL_updatesTooLong:
	default:
		return nil, fmt.Errorf("RPC: %#v", x)
	}

	return x, nil
}

func (m *Proto) sendUpdate(update TL) {
	if m.updates != nil {
		m.updates <- update
	} else {
		m.updatesQueue = append(m.updatesQueue, update)
		logs.Debug("do not send update to updates chan (chan is nil): %T-%+v", update)
	}
}

func (m *Proto) setIsSyncing(is bool) {
	m.isSyncing = is
}
