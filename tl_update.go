package mtproto

//resolution - represents the decision of update state check
//there may be applying of update, ignoring of it or there is a gap and it needs to get missed updates from remote
type resolution string

const (
	apply  resolution = "apply"
	ignore resolution = "ignore"
	gap    resolution = "gap"
)

//updateType - represents the update type, to switch between Update interfaces
//there is UpdatePts, UpdateQts, UpdateSeq and Update (the common) interfaces
type updateType string

const (
	pts   updateType = "pts"
	qts   updateType = "qts"
	seq   updateType = "seq"
	other updateType = "other" //any other update without pts, qts, seq
)

//Update - common interface for updates
//(Object which is perceived by the client without a call on its part when an event occurs.)
type Update interface {
	getDate() int32
	getType() updateType
}

//UpdatePts - common message box sequence (pts)
type UpdatePts interface {
	Update
	getPts() int32
	getSeq() int32
	getPtsCount() int32
}

//UpdateQts - secret message box sequence (qts)
type UpdateQts interface {
	Update
	getQts() int32
	getQtsCount() int32
}

//UpdateSeq - any sequential update (seq)
type UpdateSeq interface {
	Update
	getSeq() int32
	getStartSeq() int32
}

////UpdateDialogMsg - common interface for all dialog and chat messages
//type UpdateMsg interface {
//	Update
//	GetFromId() int32
//	GetChatId() int32
//	GetMessage() string
//	IsChat() bool
//}

type TL_updateShortMessage struct {
	id      int32
	from_id int32
	message string
	pts     int32
	date    int32
	seq     int32
}

func (u TL_updateShortMessage) GetFromId() int32 {
	return u.from_id
}

func (u TL_updateShortMessage) GetChatId() int32 {
	return u.from_id
}

func (u TL_updateShortMessage) GetMessage() string {
	return u.message
}

func (u TL_updateShortMessage) IsChat() bool {
	return false
}

func (u TL_updateShortMessage) getPts() int32 {
	return u.pts
}

func (u TL_updateShortMessage) getPtsCount() int32 {
	return 1
}

func (u TL_updateShortMessage) getSeq() int32 {
	return u.seq
}

func (u TL_updateShortMessage) getDate() int32 {
	return u.date
}

func (u TL_updateShortMessage) getType() updateType {
	return pts
}

type TL_updateShortChatMessage struct {
	id      int32
	from_id int32
	chat_id int32
	message string
	pts     int32
	date    int32
	seq     int32
}

func (u TL_updateShortChatMessage) GetFromId() int32 {
	return u.from_id
}

func (u TL_updateShortChatMessage) GetChatId() int32 {
	return u.chat_id
}

func (u TL_updateShortChatMessage) GetMessage() string {
	return u.message
}

func (u TL_updateShortChatMessage) IsChat() bool {
	return true
}

func (u TL_updateShortChatMessage) getPts() int32 {
	return u.pts
}

func (u TL_updateShortChatMessage) getPtsCount() int32 {
	return 1
}

func (u TL_updateShortChatMessage) getSeq() int32 {
	return u.seq
}

func (u TL_updateShortChatMessage) getDate() int32 {
	return u.date
}

func (u TL_updateShortChatMessage) getType() updateType {
	return pts
}

type TL_updateShort struct {
	update TL // Update
	date   int32
}

func (u TL_updateShort) getDate() int32 {
	return u.date
}

func (u TL_updateShort) getType() updateType {
	return other
}

type TL_updateNewMessage struct {
	message TL // Message
	pts     int32
}

func (u TL_updateNewMessage) getPts() int32 {
	return u.pts
}

func (u TL_updateNewMessage) getPtsCount() int32 {
	return 1
}

func (u TL_updateNewMessage) getSeq() int32 {
	return 0
}

func (u TL_updateNewMessage) getDate() int32 {
	return 0
}

func (u TL_updateNewMessage) getType() updateType {
	return pts
}

type TL_updateReadMessages struct {
	messages []int32
	pts      int32
}

func (u TL_updateReadMessages) getPts() int32 {
	return u.pts
}

func (u TL_updateReadMessages) getPtsCount() int32 {
	return 1
}

func (u TL_updateReadMessages) getSeq() int32 {
	return 0
}

func (u TL_updateReadMessages) getDate() int32 {
	return 0
}

func (u TL_updateReadMessages) getType() updateType {
	return pts
}

type TL_updateDeleteMessages struct {
	messages []int32
	pts      int32
}

func (u TL_updateDeleteMessages) getPts() int32 {
	return u.pts
}

func (u TL_updateDeleteMessages) getPtsCount() int32 {
	return 1
}

func (u TL_updateDeleteMessages) getSeq() int32 {
	return 0
}

func (u TL_updateDeleteMessages) getDate() int32 {
	return 0
}

func (u TL_updateDeleteMessages) getType() updateType {
	return pts
}

type TL_updateNewEncryptedMessage struct {
	message TL // EncryptedMessage
	qts     int32
}

func (u TL_updateNewEncryptedMessage) getQts() int32 {
	return u.qts
}

func (u TL_updateNewEncryptedMessage) getQtsCount() int32 {
	return 1
}

func (u TL_updateNewEncryptedMessage) getDate() int32 {
	return 0
}

func (u TL_updateNewEncryptedMessage) getType() updateType {
	return qts
}

type TL_updatesCombined struct {
	updates   []TL // Update
	users     []TL // User
	chats     []TL // Chat
	date      int32
	seq_start int32
	seq       int32
}

func (u TL_updatesCombined) getStartSeq() int32 {
	return u.seq_start
}

func (u TL_updatesCombined) getSeq() int32 {
	return u.seq
}

func (u TL_updatesCombined) getDate() int32 {
	return u.date
}

func (u TL_updatesCombined) getType() updateType {
	return seq
}

type TL_updates struct {
	updates []TL // Update
	users   []TL // User
	chats   []TL // Chat
	date    int32
	seq     int32
}

func (u TL_updates) getStartSeq() int32 {
	return u.seq
}

func (u TL_updates) getSeq() int32 {
	return u.seq
}

func (u TL_updates) getDate() int32 {
	return u.date
}

func (u TL_updates) getType() updateType {
	return seq
}
