package mtproto

import "time"

//Session - represent tlg session.
//All requests from this sessions will be proceeded within this session
//This is important to keep Encrypted=true, to avoid auth key recreation
type Session struct {
	ID           int64  `json:"session_id"`
	AuthKey      []byte `json:"auth_key"`
	AuthKeyHash  []byte `json:"auth_key_hash"`
	ServerSalt   []byte `json:"server_salt"`
	Encrypted    bool   `json:"encrypted"` //if false, new auth key will be created even for the saved session
	LastSeqNo    int32  `json:"last_seq_no"`
	SeqNo        int32  `json:"seq_no"`
	MsgId        int64  `json:"msg_id"`
	UpdatesState updatesState
}

func (s *Session) setState(state updatesState) {
	//todo properly update chan states
	s.UpdatesState = state
}

//EqualState - returns true if current state and provided state are equal (date comparing is ignored)
func (s *Session) EqualState(state updatesState) bool {
	if s.UpdatesState.Pts != state.Pts || s.UpdatesState.Qts != state.Qts || s.UpdatesState.Seq != state.Seq {
		return false
	}
	return true
}

//Account - represents tlg account info
type Account struct {
	ID       int32
	FName    string
	LName    string
	UserName string
	Photo    TL_fileLocation
}

//Dialogs - represents dialogs returned by GetDialogs()
type Dialogs struct {
	Chats []Chat //most recent chats (dialogs and chats)
	Users []User //users related to the most recent chats
}

//appendDialog - proceeds and appends tlDialog to the chats
func (d *Dialogs) appendDialog(tlDialog TL) {
	if dialog, ok := tlDialog.(TL_dialog); ok {
		if peer, ok := dialog.peer.(TL_peerUser); ok {
			d.Chats = append(d.Chats,
				Chat{
					ID:      peer.user_id,
					IsGroup: false})
		}
	}
}

//appendChat - proceeds and appends tlChat to the chats
func (d *Dialogs) appendChat(tlChat TL) {
	if chat, ok := tlChat.(TL_chat); ok {
		d.Chats = append(d.Chats,
			Chat{
				ID:      chat.id,
				Title:   chat.title,
				IsGroup: true})
	}
}

func (d *Dialogs) appendUser(tlUser TL) {
	switch tlUser.(type) {
	case TL_userSelf: //user itself
		var user TL_userSelf
		user = tlUser.(TL_userSelf)
		d.Users = append(d.Users, User{
			ID:         user.id,
			FName:      user.first_name,
			LName:      user.last_name,
			UserName:   user.username,
			AccessHash: 0,
		})
	case TL_userRequest: //user which wrote to current user
		var user TL_userRequest
		user = tlUser.(TL_userRequest)
		d.Users = append(d.Users, User{
			ID:         user.id,
			FName:      user.first_name,
			LName:      user.last_name,
			UserName:   user.username,
			AccessHash: user.access_hash,
		})
	case TL_userForeign: //user to whom current user wrote
		var user TL_userForeign
		user = tlUser.(TL_userForeign)
		d.Users = append(d.Users, User{
			ID:         user.id,
			FName:      user.first_name,
			LName:      user.last_name,
			UserName:   user.username,
			AccessHash: user.access_hash,
		})
	}
}

//Chat - represent tlg chat info
type Chat struct {
	ID      int32
	Title   string //empty for the dialogs (peer to peer, not group)
	IsGroup bool   //if true - there is group chat
}

type User struct {
	ID         int32
	FName      string
	LName      string
	UserName   string
	AccessHash int64
}

//updatesState- represent TL_updatesState
type updatesState struct {
	Pts         int32           //number of events occured in text box
	Qts         int32           //Position in a sequence of updates in secret chats.
	Date        int32           //Date of condition
	Seq         int32           //Number of sent updates
	UnreadCount int32           //Number of unread messages
	ChannelsPts map[int32]int32 //chan_id->chan_pts (there is pts for tlg channels)
}

type packetToSend struct {
	msg  TL
	resp chan TL
}

type Conf struct {
	AppID   int32
	AppHash string
	TlgAddr string
	//Timeout - timeout for read routine
	//(if no data received from conn within this duration, read routine returns)
	Timeout time.Duration
}
