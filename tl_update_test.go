package mtproto

import (
	"testing"
)

func TestProcessUpdateState(t *testing.T) {
	testUpdate := TL_updates{
		updates: []TL{
			TL_updateNewMessage{
				message: TL_message{
					flags:   258,
					id:      205,
					from_id: 876174123,
					to_id:   TL_peerUser{user_id: 571029719},
					date:    1568947943,
					message: "Ответ 322",
					media:   TL_messageMediaEmpty{},
				},
				pts: 322},
		}, users: []TL{
			TL_userSelf{
				id:         876174123,
				first_name: "Владлен",
				last_name:  "Тестовый",
				username:   "",
				phone:      "77051023471",
				photo:      TL_userProfilePhotoEmpty{},
				status:     TL_userStatusOnline{expires: 1568948217},
				inactive:   TL_boolFalse{},
			},
			TL_userForeign{
				id:          571029719,
				first_name:  "Владимир",
				last_name:   "Савостин",
				username:    "Savostin_Vladimir",
				access_hash: -936177933186537332,
				photo: TL_userProfilePhoto{photo_id: 2452553968605308842,
					photo_small: TL_fileLocation{dc_id: 2, volume_id: 250934170, local_id: 47244, secret: 7995313537947463310},
					photo_big:   TL_fileLocation{dc_id: 2, volume_id: 250934170, local_id: 47246, secret: 5500000763981190102}},
				status: TL_userStatusOffline{was_online: 1568947919},
			},
		},
		chats: []TL{},
		date:  1568947942,
		seq:   12}

	p, err := NewMTProto(Conf{})
	if err != nil {
		t.Fatal(err)
	}
	p.session.setState(updatesState{
		Pts:         320,
		Qts:         0,
		Date:        1568947942,
		Seq:         10,
		UnreadCount: 0,
		ChannelsPts: nil,
	})
	p.checkState()
}
