package main

import (
	"bitbucket.org/vladimir_savostin/mtproto"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/logs"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func usage() {
	fmt.Print("Telegram is a simple MTProto tool.\n\nUsage:\n\n")
	fmt.Print("    ./telegram <command> [arguments]\n\n")
	fmt.Print("The commands are:\n\n")
	fmt.Print("    auth  <phone_number>            auth connection by code\n")
	fmt.Print("    msg   <user_id> <msgtext>       send message to user\n")
	fmt.Print("    list                            get contact list\n")
	fmt.Println()
}

const (
	appId       = 967394
	appHash     = "5cfbb1fca47b0b797d675b893392c0e4"
	tlgAddr     = "149.154.167.50:443"
	sessionPath = "session.json"

	phoneTest    = "+77088406842"
	codeTest     = "37733"
	codeHashTest = "a81cc3b588bfdeec33"
)

func main() {

	logs.SetLogFuncCall(true)
	logs.SetLogFuncCallDepth(3)
	//create new client with clear session
	logs.Info("create new client")
	var m mtproto.MTProto
	m, err := mtproto.NewMTProto(mtproto.Conf{
		AppID:   appId,
		AppHash: appHash,
		TlgAddr: tlgAddr,
		Timeout: time.Second * 300,
	})
	if err != nil {
		fmt.Printf("Create failed: %s\n", err)
		os.Exit(2)
	}
	defer func() {
		err := recover()
		if err != nil {
			err := m.Close()
			if err != nil {
				fmt.Printf("close conn: %s\n", err)
			}
			session := m.GetSession()
			err = saveSession(session)
			if err != nil {
				fmt.Printf("save session: %s\n", err)
				os.Exit(2)
			}
		}

	}()
	defer func() {
		err := m.Close()
		if err != nil {
			fmt.Printf("close conn: %s\n", err)
		}
		session := m.GetSession()
		err = saveSession(session)
		if err != nil {
			fmt.Printf("save session: %s\n", err)
			os.Exit(2)
		}
	}()

	session, err := loadSession(sessionPath)
	if err == nil {
		logs.Debug("load session")
		m.SetSession(session)
		err = m.Connect()
		if err != nil {
			fmt.Printf("Connect failed: %s\n", err)
			return
		}
	} else {
		logs.Debug("create new session")
		err := authorize(m)
		if err != nil {
			logs.Error(err)
			return
		}
	}

	SetupCloseHandler(m)
	go func() {
		logs.Info("start listening for updates")
		for update := range m.GetUpdates() {
			logs.Info("there is new update: %#v", update)
		}
	}()

	state, err := m.GetState()
	if err != nil {
		logs.Error(err)
	}
	if !m.GetSession().EqualState(state) {
		logs.Warn("there is not equal state\nlocal:%+v\nremote%+v",
			m.GetSession().UpdatesState, state)
		logs.Info("start to sync updates")
		err := m.SyncDifference()
		if err != nil {
			logs.Error(err)
		}
	}
	////test send msg
	//err = m.SendToUser(571029719, -936177933186537332, "this is a test msg to user")
	//logs.Error(err)
	//err = m.SendToChat(275765586, "this is a test msg to chat")
	//logs.Error(err)
	logs.Debug("started with state:%+v", m.GetSession().UpdatesState)
	for {
		logs.Info("please type the command:")
		var command string
		_, err = fmt.Scanf("%s", &command)
		if err != nil {
			logs.Error(err)
		}
		switch command {
		case "dialogs":
			chats, err := m.GetDialogs(100)
			if err != nil {
				fmt.Printf("get dialogs failed: %s\n", err)
				os.Exit(2)

			}
			logs.Debug("%+v", chats)
		case "state":
			s, err := m.GetState()
			if err != nil {
				logs.Error(err)
				continue
			}
			logs.Info("%+v", s)
		case "diff":
			diff, err := m.GetDifference()
			if err != nil {
				logs.Error(err)
				continue
			}
			logs.Info("%+v", diff)
		default:
			logs.Error("unknown command: type [dialogs | state]")
		}
	}
}

func saveSession(s *mtproto.Session) error {
	buf, err := json.Marshal(s)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile("session.json", buf, 0644)
	if err != nil {
		return err
	}
	return nil
}

func loadSession(path string) (session *mtproto.Session, err error) {
	session = new(mtproto.Session)
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}
	err = json.Unmarshal(buf, session)
	if err != nil {
		return
	}
	return session, nil
}

func authorize(m mtproto.MTProto) error {
	err := m.Connect()
	if err != nil {
		fmt.Printf("Connect failed: %s\n", err)
		return err
	}
	fmt.Println("\nenter phone number:")
	var phoneNumber string
	_, err = fmt.Scanf("%s", &phoneNumber)
	if err != nil {
		fmt.Printf("wrong number: %s\n", err)
		return err
	}

	registered, err := m.AuthCheckPhone(phoneNumber)
	if err != nil {
		fmt.Printf("check phone failed: %s\n", err)
		return err
	}
	if !registered {
		fmt.Printf("phone %s is not registered", phoneNumber)
		os.Exit(2)
	}
	//send code
	codeHash, err := m.AuthSendCode(phoneNumber)
	if err != nil {
		fmt.Printf("auth failed: %s\n", err)
		return err
	}

	fmt.Println("\nenter auth code:")
	var code string
	_, err = fmt.Scanf("%s", &code)
	if err != nil {
		fmt.Printf("wrong number: %s\n", err)
		return err
	}

	account, err := m.AuthSignIn(phoneNumber, code, codeHash)
	logs.Debug(account)
	if err != nil {
		fmt.Printf("sign in failed: %s\n", err)
		return err
	}
	return nil
}

func SetupCloseHandler(m mtproto.MTProto) {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		err := m.Close()
		if err != nil {
			fmt.Printf("close conn: %s\n", err)
		}
		session := m.GetSession()
		err = saveSession(session)
		if err != nil {
			fmt.Printf("save session: %s\n", err)
			os.Exit(2)
		}
		os.Exit(2)
	}()
}
